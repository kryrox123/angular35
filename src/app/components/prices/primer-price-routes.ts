import { Routes } from "@angular/router";
import { PrimerMondayComponent } from "./primer-monday.component";
import { PrimerPriceComponent } from "./primer-price.component";
import { PrimerSundayComponent } from "./primer-sunday.component";
import { PrimerTuesdayComponent } from "./primer-tuesday.component";
import { SegundoPriceComponent } from "./segundo-price.component";
import { TercerPriceComponent } from "./tercer-price.component";


export const days_Routes: Routes =[
  { path: "sunday", component: PrimerSundayComponent },
  { path: "monday", component: PrimerMondayComponent },
  { path: "tuesday", component: PrimerTuesdayComponent },
  { path: "**", pathMatch: "full", redirectTo: "sunday" }
]
